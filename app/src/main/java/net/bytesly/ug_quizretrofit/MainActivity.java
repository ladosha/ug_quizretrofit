package net.bytesly.ug_quizretrofit;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import net.bytesly.ug_quizretrofit.model.PlaceholderPost;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppController.getInstance().getRetrofitClient().getApiService().getPosts().enqueue(new Callback<List<PlaceholderPost>>() {

            @Override
            public void onResponse(Call<List<PlaceholderPost>> call, Response<List<PlaceholderPost>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    List<PlaceholderPost> posts = response.body();
                    for (int i = 0; i < posts.size(); i++) {
                        Log.d("MyPosts", posts.get(i).toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<List<PlaceholderPost>> call, Throwable t) {

            }

        });
    }
}