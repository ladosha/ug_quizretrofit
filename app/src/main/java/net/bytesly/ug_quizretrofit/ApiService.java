package net.bytesly.ug_quizretrofit;

import net.bytesly.ug_quizretrofit.model.PlaceholderPost;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    @GET("posts")
    Call<List<PlaceholderPost>> getPosts();
}
