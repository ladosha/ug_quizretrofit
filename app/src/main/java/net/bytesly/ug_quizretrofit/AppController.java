package net.bytesly.ug_quizretrofit;

import android.app.Application;

public class AppController extends Application {

    private static AppController instance;

    private RetrofitClient retrofitClient;

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;

        retrofitClient = new RetrofitClient();
    }

    public static AppController getInstance() {
        return instance;
    }

    public RetrofitClient getRetrofitClient() {
        return retrofitClient;
    }
}
